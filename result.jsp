<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
    <title>News</title>
</head>
<body>

    <c:forEach items="${listNews}" var="item">
        <p>
            <c:out value="${item.title}" />
        </p>
        <p>
            <a href="<c:out value="${requestScope['javax.servlet.forward.request_uri']}" />?id=<c:out value = "${item.id}" />&format=html">html</a> |
            <a href="<c:out value="${requestScope['javax.servlet.forward.request_uri']}" />?id=<c:out value = "${item.id}" />&format=json">json</a> |
            <a href="<c:out value="${requestScope['javax.servlet.forward.request_uri']}" />?id=<c:out value = "${item.id}" />&format=xml">xml</a> |
        </p>
        <p>
            <a href="<c:out value="${requestScope['javax.servlet.forward.request_uri']}" />?id=<c:out value = "${item.id}" />&action=edit">edit</a> |
            <form action="List" method="post">
                <input type="hidden" name="id" value='<c:out value="${item.id}"/>' />
                <input type="hidden" name="delete" value='<c:out value="1"/>' />
                <input type="submit" value="delete"/>
                <%--<a href="/News/List?id=<c:out value = "${item.id}" />&action=delete">delete</a>--%>
            </form>

        </p>
        <hr>
    </c:forEach>


<div style="">
    <a href="formNews.html">Add new news</a>
</div>
</body>
</html>
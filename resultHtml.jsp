<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
    <title>News</title>
</head>
<body>

<ul>
<p>Title:  <c:out value="${news.title}" /></p>
<p>Text:   <c:out value="${news.body}" /></p>
<p>Author: <c:out value="${news.author}" /></p>
<p>Date of Publish: <c:out value="${news.datePublished}" /></p>
</ul>

<div style="">
    <a href="formNews.html">Add new news</a>

</div>
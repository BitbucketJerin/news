package com.news.dao;

import java.io.*;
import java.util.*;
import java.sql.*;
import com.news.model.*;

public class ProcessDataNews {

    static final String JDBC_DRIVER="com.mysql.jdbc.Driver";
    static final String DB_URL="jdbc:mysql://localhost/news";

    //  Database credentials
    static final String USER = "root";
    static final String PASS = "123456";

    /**
     *
     * @return
     */
    public List<News> fetchNews() {
        int id = 0;
        PreparedStatement preparedStatement = null;
        Connection dbConnection = null;
        String sql = "";
        List<News> listNews = new ArrayList<News>();

        try {
            dbConnection = getConnectionDb();
            sql = "SELECT id, title FROM news_details where status = ?";

            preparedStatement = dbConnection.prepareStatement(sql);
            preparedStatement.setInt(1, News.ACTIVE);
            ResultSet rs = preparedStatement.executeQuery();

            while(rs.next()){
                News news = new News();
                news.setId(rs.getInt("id"));
                news.setTitle(rs.getString("title"));

                listNews.add(news);
            }

        } catch (SQLException se) {
            //Handle errors for JDBC
            se.printStackTrace();
        } finally {
            try {
                if (preparedStatement!=null) {
                    preparedStatement.close();
                }
            } catch (SQLException se2) {

            }// nothing we can do
            try {
                if (dbConnection!=null) {
                    dbConnection.close();
                }
            } catch (SQLException se) {
                se.printStackTrace();
            }//end finally try
        }

        return listNews;
    }

    /**
     *
     * @param news
     * @return
     */
    public String saveNews(News news) {
        Connection dbConnection             = null;
        PreparedStatement preparedStatement = null;
        String insertTableSQL               = "";

        try {
            dbConnection = getConnectionDb();

            insertTableSQL = "INSERT INTO news_details"
                    + "(title, body, author, published_date) VALUES"
                    + "(? ,?, ?, ?)";


            preparedStatement = dbConnection.prepareStatement(insertTableSQL);

            java.util.Date date = news.getDatePublished();
            java.sql.Date dateSql = null;

            if (date != null) {
                dateSql = getDateSql(date);
            }

            preparedStatement.setString(1, news.getTitle());
            preparedStatement.setString(2, news.getBody());
            preparedStatement.setString(3, news.getAuthor());
            preparedStatement.setDate(4, dateSql);

            // execute insert SQL stetement
            int rows = preparedStatement.executeUpdate();

            insertTableSQL = "Rows impacted : " + rows;

        } catch(SQLException se) {
            //Handle errors for JDBC
            se.printStackTrace();
        } finally {
            //finally block used to close resources
            try {
                if(preparedStatement!=null) {
                    preparedStatement.close();
                }
            } catch (SQLException se2 ){

            }// nothing we can do
            try{
                if(dbConnection!=null) {
                    dbConnection.close();
                }
            } catch (SQLException se) {
                se.printStackTrace();
            }//end finally try
        }

        return insertTableSQL;
    }

    private java.sql.Date getDateSql(java.util.Date date) {

        return new java.sql.Date(date.getTime());

    }

    /**
     *
     * @param id
     * @return
     */
    public News fetchNewsById(int id) {
        PreparedStatement preparedStatement = null;
        Connection dbConnection             = null;
        String sql                          = "";
        News news                           = new News();

        try {

            dbConnection = getConnectionDb();

            sql = "SELECT title, body, author, published_date FROM news_details where id = ? and status = ?";

            preparedStatement = dbConnection.prepareStatement(sql);
            preparedStatement.setInt(1, id);
            preparedStatement.setInt(2, 1);

            ResultSet rs = preparedStatement.executeQuery();

            while (rs.next()) {
                java.util.Date date = rs.getDate("published_date");

                news.setTitle(rs.getString("title"));
                news.setBody(rs.getString("body"));
                news.setAuthor(rs.getString("author"));
                news.setDatePublished(date);
                news.setId(id);
            }
        } catch (SQLException se) {
            //Handle errors for JDBC
            se.printStackTrace();
        } finally {
            //finally block used to close resources
            try {
                if (preparedStatement!=null) {
                    preparedStatement.close();
                }
            } catch (SQLException se2) {

            }// nothing we can do
            try {
                if (dbConnection!=null) {
                    dbConnection.close();
                }
            } catch (SQLException se) {
                se.printStackTrace();
            }//end finally try
        } //end try

        return news;
    }

    /**
     *
     * @param news
     * @param idNews
     */
    public void updateNews(News news, int idNews) {
        Connection dbConnection             = null;
        PreparedStatement preparedStatement = null;
        String sql                          = "";

        try {
            dbConnection = getConnectionDb();

            sql = "UPDATE news_details SET title = ?, body = ?, author = ?, published_date = ? WHERE id = ?";

            //String sql = "UPDATE Employees set age=? WHERE id=?";
            preparedStatement = dbConnection.prepareStatement(sql);

            java.util.Date date = news.getDatePublished();
            java.sql.Date dateSql = null;

            if (date != null) {
                dateSql = getDateSql(date);
            }

            preparedStatement.setString(1, news.getTitle());
            preparedStatement.setString(2, news.getBody());
            preparedStatement.setString(3, news.getAuthor());
            preparedStatement.setDate(4, dateSql);
            preparedStatement.setInt(5, idNews);
            // execute insert SQL stetement
            int rows = preparedStatement.executeUpdate();

        } catch (SQLException se) {
            //Handle errors for JDBC
            se.printStackTrace();
        } finally {
            //finally block used to close resources
            try {
                if(preparedStatement != null) {
                    dbConnection.close();
                }
            } catch (SQLException se) {

            }// do nothing
            try {
                if(dbConnection != null) {
                    dbConnection.close();
                }
            } catch (SQLException se) {
                se.printStackTrace();
            }//end finally try
        }//end try

    }

    /**
     *
     * @param id
     */
    public void deleteNews(int id) {
        PreparedStatement preparedStatement = null;
        Connection dbConnection = null;
        String sql = "";

        try {
            dbConnection = getConnectionDb();
            sql = "UPDATE news_details SET status = ? WHERE id = ?";

            preparedStatement = dbConnection.prepareStatement(sql);

            //preparedStatement.setInt(1, 1);
            preparedStatement.setInt(1, News.REMOVED);
            preparedStatement.setInt(2, id);

            int rows = preparedStatement.executeUpdate();

            sql = "Rows impacted : " + rows;

        } catch (SQLException se) {
            //Handle errors for JDBC
            se.printStackTrace();
        } finally {
            //finally block used to close resources
            try {
                if(preparedStatement != null) {
                    dbConnection.close();
                }
            } catch(SQLException se) {

            }// do nothing
            try {
                if (dbConnection != null) {
                    dbConnection.close();
                }
            } catch (SQLException se) {
                se.printStackTrace();
            }//end finally try
        }//end try
    }

    /**
     * 
     * @return
     */
    private Connection getConnectionDb() {
        Connection dbConnection = null;

        try {
            Class.forName(JDBC_DRIVER);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        try {
            dbConnection = DriverManager.getConnection(DB_URL, USER, PASS);
            return dbConnection;

        } catch (SQLException se) {
            se.printStackTrace();
        }

        return dbConnection;
    }
}
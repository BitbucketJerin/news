package com.news.model;

import java.sql.*;
import java.util.*;
import java.text.*;


public class News {

    int id;
    String title;
    String body;
    String author;
    String status;
    java.util.Date datePublished;

    public static final int ACTIVE   = 1;
    public static final int REMOVED  = 0;

    public void setId(int id) {

        this.id = id;
    }

    public void setTitle(String title) {

        this.title = title;
    }

    public void setBody(String body) {

        this.body = body;
    }

    public void setDatePublished (java.util.Date datePublished) {

        this.datePublished = datePublished;

    }

    public void setAuthor(String author) {

        this.author = author;
    }

    public int getId() {

        return this.id;
    }

    public String getTitle() {

        return this.title;
    }

    public String getBody() {

        return this.body;
    }

    public String getAuthor() {

        return this.author;
    }

    public java.util.Date getDatePublished() {

        return this.datePublished;
    }

    public java.util.Date convertDate(String dateInput) {

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");  //@todo: direct conversion to sql date?
        java.util.Date date = null;

        try {
            date = dateFormat.parse(dateInput);

        } catch (Exception e) {

        }

        return date;

    }

}
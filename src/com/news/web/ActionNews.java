package com.news.web;

import com.news.model.*;
import com.news.dao.*;
import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;
import java.util.*;
import com.google.gson.Gson;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.Marshaller;
import javax.xml.namespace.QName;
import javax.xml.bind.JAXBException;


public class ActionNews extends HttpServlet {

    /**
     * add, edit and delete [/List]
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String title     = request.getParameter("title");
        String body      = request.getParameter("body");
        String author    = request.getParameter("author");

        String datePublished = request.getParameter("date-publish");
        String idNews        = request.getParameter("id");  //to delete and edit

        String performDelete = request.getParameter("delete");  //to delete

        News news = new News();

        java.util.Date date = news.convertDate(datePublished);
        news.setTitle(title);
        news.setBody(body);
        news.setAuthor(author);
        news.setDatePublished(date);

        ProcessDataNews process = new ProcessDataNews();

        if (idNews != null && !idNews.isEmpty()) {
            int idNewsToUpdate = Integer.parseInt(idNews);

            if (performDelete != null && performDelete.equals("1")) {
                process.deleteNews(idNewsToUpdate);
            } else {
                process.updateNews(news, idNewsToUpdate);
            }

        } else {
            String sql = process.saveNews(news);
        }


        //PrintWriter out = response.getWriter();
        //out.println(news.getDatePublished());

        List<News> listNews = new ArrayList<News>();

        listNews = process.fetchNews();

        request.setAttribute("listNews", listNews);
        getServletConfig().getServletContext().getRequestDispatcher("/result.jsp").forward(request,response);

    }

    /**
     * Show List of news, specific one news with different format
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String idNews = request.getParameter("id"); //News id

        String format = request.getParameter("format");
        String action = request.getParameter("action");
        ProcessDataNews process = new ProcessDataNews();

        PrintWriter out = response.getWriter();

        if (idNews != null && !idNews.isEmpty()) {
            int id = Integer.parseInt(idNews);

            if (id > 0) {
                News news = process.fetchNewsById(id);

                if (format != null && !format.isEmpty()) {

                    switch (format) {
                        case("json"):
                            Gson gson       = new Gson();
                            String newsJson = gson.toJson(news);
                            response.setContentType("application/json");
                            response.setCharacterEncoding("UTF-8");

                            response.getWriter().write(newsJson);
                            break;

                        case("xml"):
                            try {
                                response.setHeader("Content-Type", "text/xml; charset=UTF-8");
                                JAXBContext jc = JAXBContext.newInstance(News.class);

                                Marshaller marshaller = jc.createMarshaller();
                                marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
                                JAXBElement<News> jaxbElement = new JAXBElement<News>(new QName("news"), News.class, news);
                                marshaller.marshal(jaxbElement, out);
                            } catch (JAXBException e) {
                                e.printStackTrace();
                            }

                            break;

                        default:
                            request.setAttribute("news", news);
                            getServletConfig().getServletContext().getRequestDispatcher("/resultHtml.jsp").forward(request,response);
                    }

                } else if (action != null && !action.isEmpty()) {
                    if (action.equals("edit")) {
                        request.setAttribute("news", news);
                        getServletConfig().getServletContext().getRequestDispatcher("/formEditNews.jsp").forward(request, response);
                    }
                } else { // default
                    request.setAttribute("news", news);
                    getServletConfig().getServletContext().getRequestDispatcher("/resultHtml.jsp").forward(request,response);
                }
            }
        } else {
            List<News> listNews = new ArrayList<News>();

            listNews = process.fetchNews();

            request.setAttribute("listNews", listNews);
            getServletConfig().getServletContext().getRequestDispatcher("/result.jsp").forward(request, response);
        }

    }

}
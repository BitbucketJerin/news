<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
    <title>News EDIT</title>
</head>
<body>

<form action="List" method="post">
    <p>Title: <input type="text" name="title" value='<c:out value="${news.title}"/>' /></p>

    <p>Body: <textarea  name="body"><c:out value="${news.body}" /></textarea></p>


    <p>Author: <input type="text" size="50" name="author" value='<c:out value="${news.author}"/>' /></p>

    <p>Date of Publish: <input type="text" size="50" name="date-publish" value='<c:out value="${news.datePublished}"/>' /></p>

    <input type="hidden" name="id" value='<c:out value="${news.id}"/>' />
    <input type="submit" value="submit" />
</form>

</body>
</html>
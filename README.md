# README #

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

SQL for database:
Get the database script from the following link: https://bitbucket.org/BitbucketJerin/news/src/f3d0a7812768/database/?at=master

Steps to check:
1. cd /var/lib/tomcat7/webapps
2. git clone https://BitbucketJerin@bitbucket.org/BitbucketJerin/news.git
3. Rename the folder to News from news.
4. URLS: To view news:
/News/List

Used library:
gson [for json manipulation]
jstl [for view in jsp]
mysql-connector [for jdbc]